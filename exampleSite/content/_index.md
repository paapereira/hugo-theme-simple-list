---
title: Home
---

# Hugo Simple List Theme

Simple List is forked from the **hugo-classic** theme, written by [goodroot](https://goodroot.ca/). It's alson inspired by the [terminal](https://github.com/panr/hugo-theme-terminal) theme.

[**View live demo in my homepage**](https://paapereira.xyz/)

---

### Blog posts
