# Hugo - Simple List Theme

Simple List is forked from the [hugo-classic](https://github.com/goodroot/hugo-classic) theme, written by [goodroot](https://goodroot.ca/). It's also inspired by the [terminal](https://github.com/panr/hugo-theme-terminal) theme.

[**View live demo in my homepage**](https://paapereira.xyz/)

### Instructions

Based on an [Arch Linux](https://archlinux.org/) based OS. For a few more tips on how to setup a Hugo based site you can check my [blog](https://paapereira.xyz/posts/2020/04/setting-hugo-with-gitlab/).

1: Install Hugo.

```
sudo pacman -S hugo
```

2: Create a new site.

```
hugo new site mySimpleListHomepage
cd mySimpleListHomepage
git init
```

3: Clone the repo.

```
git submodule add https://gitlab.com/paapereira/hugo-theme-simple-list themes/simple-list
```

4: Copy files within the `exampleSite/` directory into the `mySimpleListHomepage/` directory. Overwrites the existing `content/`, `static/`, and `config.toml` files.

```
cp -r themes/simple-list/exampleSite/* .
```

5: Run `hugo` within `mySimpleListHomepage/` and enjoy and customize to your hearts content!

```
hugo server
```

### New Posts

To make new posts, simply use the command line:

```
hugo new posts/good-to-great.md
```

#### Screenshot

![Screenshot](/images/screenshot.png)

### TODO

- [X] Retake screenshots

- [X] Still some tweaks to be made in the style.css (like tables, code blocks and post titles)

- [X] Fix the menu bar in a mobile situation (the right most items are hidden)

- [X] Support for pagination in the posts lists

- [ ] Add the theme to https://gohugo.io/contribute/themes/